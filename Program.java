/*
 * Program.java        20.10.2020
 *
 * Program to calculate the area and the perimeter
 * of a circle knowing the radius in meter.
 *
 * Copyright 2020 Raul Gomez . <raull.gomez.eso12@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class Program {
/**
* Calculates the area of a circle
*
* @param radius is a double numbers
* @return a double: the area of a circle
*/
public static double area(double radius) {

return Math.PI * Math.pow(radius, 2);
}

/**
* Calculates the perimeter of a circle
*
* @param radius is a double numbers
* @return a double: the perimeter of a circle
*/
public static double perimeter(double radius) {

return (2 * Math.PI) * radius;
}

/**
* TUI = Terminal User Interface
*
* @param args not used.
*/
public static void main(String[] args) {


Scanner in = new Scanner(System.in);
in.useLocale(Locale.FRENCH);

System.out.println("Enter the radius in meterss: ");
double radi = in.nextDouble();

in.close();

System.out.println("\n\nThe area is: " + ((double)Math.round(area(radi)* 100d) / 100d + "m"));

System.out.println("\n\nThe perimeter is: " + ((double)Math.round(perimeter(radi)* 100d) /100d + "m"));

}
}
